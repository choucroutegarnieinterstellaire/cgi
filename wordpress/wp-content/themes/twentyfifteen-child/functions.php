<?php // Balise d'ouverture PHP - il ne doit rien avoir avant cela m�me pas un espace

// Fonction personnalis�e � inclure
function favicon_link() {
	echo '<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />' . "\n";
}
add_action( 'wp_head', 'favicon_link' );

function projet_module() {
	$args = array(
		'label' => __('Annonces'),
		'singular_label' => __('Annonce'),
		'public' => true,
		'show_ui' => true,
		'_builtin' => false, // It's a custom post type, not built in
		'_edit_link' => 'post.php?post=%d',
		'capability_type' => 'post',
		'hierarchical' => false,
		'rewrite' => array("slug" => "petitesannonces"),
		'query_var' => "petitesannonces", // This goes to the WP_Query schema
		'supports' => array('title', 'editor', 'thumbnail', 'comment') //titre + zone de texte + champs personnalis�s + miniature valeur possible : 'title','editor','author','thumbnail','excerpt'
	);
	register_post_type( 'petitesannonces' , $args ); // enregistrement de l'entit� projet bas� sur les arguments ci-dessus
	register_taxonomy_for_object_type('post_tag', 'mots_clefs','show_tagcloud=1&hierarchical=true'); // ajout des mots cl�s pour notre custom post type
	//add_action("admin_init", "admin_init"); function pour ajouter des champs personnalis�s
	//add_action('save_post', 'save_custom'); function pour la sauvegarde de nos champs personnalis�s
}
add_action('init', 'projet_module');

function admin_init(){ //initialisation des champs sp�cifiques
	add_meta_box("Prix", "Prix", "Prix", "petitesannonces", "normal", "low"); //il sagit de notre champ personnalis qui apelera la fonction url_projet()
}
function Prix(){ //La fonction qui affiche notre champs personnalis� dans l'administration
	global $post;
	$custom = get_post_custom($post->ID); //fonction pour r�cup�rer la valeur de notre champ
	$Prix = $custom["Prix"][0];
	?>
	<input size="70" type="text" value="<?php echo $Prix;?>" name="Prix"/>
	<?php
}
function save_custom(){ //sauvegarde des champs sp�cifiques
	global $post;
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) { // fonction pour �viter� le vidage des champs personnalis�s lors de la sauvegarde automatique
		return $postID;
	}
	update_post_meta($post->ID, "Prix", $_POST["Prix"]); //enregistrement dans la base de donn�es
}

add_action("admin_init", "admin_init"); 
add_action('save_post', 'save_custom'); 

$labelsCat1 = array(
'name' => _x( 'Clients', 'post type general name' ),
'singular_name' => _x( 'Client', 'post type singular name' ),
'add_new' => _x( 'Add New', 'client' ),
'add_new_item' => __( 'Ajouter un client' ),
'edit_item' => __( 'Modifier le client' ),
'new_item' => __( 'Nouveau client' ),
'view_item' => __( 'Voir le client' ),
'search_items' => __( 'Rechercher des clients' ),
'not_found' => __( 'Aucun client trouv�' ),
'not_found_in_trash' => __( 'Aucun client trouv�' ),
'parent_item_colon' => ''
);
register_taxonomy("clients", array("projet"), array("hierarchical" => true, "labels" => $labelsCat1, "rewrite" => true));

add_theme_support( 'custom-header' );


$args = array(
	'width'         => 980,
	'height'        => 60,
	'default-image' => get_template_directory_uri() . '/images/header.jpg',
);
add_theme_support( 'custom-header', $args );