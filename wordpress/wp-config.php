<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clefs secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur 
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C'est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d'installation. Vous n'avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'wordpress');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', '');

/** Adresse de l'hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8');

/** Type de collation de la base de données. 
  * N'y touchez que si vous savez ce que vous faites. 
  */
define('DB_COLLATE', '');

/**#@+
 * Clefs uniques d'authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant 
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n'importe quel moment, afin d'invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'dJ28{PM~oE8x:5(.]+8?_yfV>BO9g`3^^y;kt9n)>m/E#y)I/$n9TQI<<XGJfP`n');
define('SECURE_AUTH_KEY',  '2tDs5dBgbihJlOHP0vM>PDa1!|,0a,@@1Cx_1EB:-g+lHHg|qP#IgU|pI2|,&|P|');
define('LOGGED_IN_KEY',    'PD-uPvSj5dfO4IR|w-<JO3T~xJM]:!-^,/W6D%M>s&x?jf|NX 5[,#35yA.w0>S^');
define('NONCE_KEY',        '+)f?*qZh7=|wR<0=-ziA,jE#==/zS|uOejxf_#R2`q1L[FM,Aj4P6:G=5$d&LB}9');
define('AUTH_SALT',        '@ZD?Q-jZe:yM<i/zT6T>+h[2+lZS^/g7*L6=/:/hA_`V#G42Uv)8@W>j|SkAv3C?');
define('SECURE_AUTH_SALT', 'AK@a}<qyrFO,r$Q]YuMMC<IYI.ir6qxO1)nt!%M{~xn,5u?NWN9]! &Unl(+?BGW');
define('LOGGED_IN_SALT',   '3Ub[Fu`>n0,3aXE<[z?S46-OLcS:nwbhcR9e#r_$EwJ!9#/Nk#gL|UP@@D2}JU K');
define('NONCE_SALT',       ':|Iu~<gZtc`=Cyw1ECaT#H~!-o3bU-AZ?+8-~A}J-1B:[?XAdH|pwvDDhuDO|?]M');
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', true);
define('DOMAIN_CURRENT_SITE', 'choucroute.taf');
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique. 
 * N'utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés!
 */
$table_prefix  = 'wp_';

/** 
 * Pour les développeurs : le mode deboguage de WordPress.
 * 
 * En passant la valeur suivante à "true", vous activez l'affichage des
 * notifications d'erreurs pendant votre essais.
 * Il est fortemment recommandé que les développeurs d'extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de 
 * développement.
 */ 
define('WP_DEBUG', false); 

/* C'est tout, ne touchez pas à ce qui suit ! Bon blogging ! */
/* Multisite */
define( 'WP_ALLOW_MULTISITE', true );

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');

